//
//  ItemTableViewCell.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    
   
    @IBOutlet weak var ImageLogo: UIImageView!
    @IBOutlet weak var ImageItem: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
     @IBOutlet weak var DeveloperLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewLabel.text = "=========>"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
