//
//  DescriptionTableViewCell.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var ImageDropdown: UIImageView!
    @IBOutlet weak var secondViewDescriptionLabel: UILabel!
    @IBOutlet weak var secondViewViewLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
