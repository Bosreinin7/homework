//
//  YoutubeViewController.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class YoutubeViewController: UITableViewController {
    var model:[YoutubeModel] = [
             YoutubeModel(youtubeImage: "image4", description: "Best Chinese Songs collection - 十三首 Romance songs 2017", developer: "Zaib Akhtar", views: "200 views", hour: "2 hours ago"),
             YoutubeModel(youtubeImage: "image1", description: "Sad Chinese Music That Will Make You Cry | Best Sad Chinese Melody Songs", developer: "Zaib Akhtar", views: "210 views", hour: "1 hours ago"),
             YoutubeModel(youtubeImage: "image2", description: "Best Chinese Songs collection - 十三首 Romance songs 2017", developer: "Zaib Akhtar", views: "211 views", hour: "3 hours ago")]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let youTubeBarButton: UIBarButtonItem = {
            let youTubeButton = UIButton()
            let logoImage = UIImage(named: "youtube")
            let scale = 110 / (logoImage?.size.width)!
            let newHeight = logoImage!.size.height * scale
            UIGraphicsBeginImageContext(CGSize(width: 110, height: newHeight))
            logoImage!.draw(in: CGRect(x: 0, y: 0, width: 110, height: newHeight))
            let ImageCustom = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            youTubeButton.setImage(#imageLiteral(resourceName: "youtube"), for: .normal)
            
            let youTubeBarButtons = UIBarButtonItem(customView: youTubeButton)
            return youTubeBarButtons
        }()
        self.navigationItem.leftBarButtonItem = youTubeBarButton

    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("ItemTableViewCell", owner: self, options: nil)?.first as! ItemTableViewCell
        cell.ImageItem.image = UIImage(named: model[indexPath.row].youtubeImage!)
        cell.ImageLogo.image = #imageLiteral(resourceName: "logo")
        cell.ImageLogo.layer.masksToBounds = true
        cell.ImageLogo.layer.cornerRadius = (cell.ImageLogo.frame.width)/2
        cell.descriptionLabel.text = model[indexPath.row].decription
        cell.DeveloperLabel.text = model[indexPath.row].developer
        cell.viewLabel.text = model[indexPath.row].views
        cell.hourLabel.text = model[indexPath.row].hour
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "SecondStoryboard") as! SecondViewController
        detail.selectedImage = UIImage(named:model[indexPath.row].youtubeImage!)
        detail.Description = model[indexPath.row].decription
        detail.Developer = model[indexPath.row].developer
        detail.Views = model[indexPath.row].views
        
        self.navigationController?.pushViewController(detail, animated: true)
    }
}
