//
//  YouTubeModel.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation
class YoutubeModel {
    var youtubeImage:String?
    var decription:String?
    var developer:String?
    var views:String?
    var hour: String?
    init(youtubeImage:String,description:String,developer:String,views:String,hour:String) {
        self.youtubeImage = youtubeImage
        self.decription = description
        self.developer = developer
        self.views = views
        self.hour = hour
    }
}
