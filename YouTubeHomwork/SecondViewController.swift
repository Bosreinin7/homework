//
//  SecondViewController.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var ImageDescription: UIImageView!
    var selectedImage: UIImage?
    var Description:String?
    var Views:String?
    var Developer:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.ImageDescription.image = self.selectedImage


        
        
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:let cell0 = Bundle.main.loadNibNamed("DescriptionTableViewCell", owner: self, options: nil)?.first as? DescriptionTableViewCell
        cell0?.secondViewDescriptionLabel.text = self.Description
        cell0?.secondViewViewLabel.text = self.Views
        cell0?.separatorInset = UIEdgeInsets.zero
        cell0?.layoutMargins = UIEdgeInsets.zero
        
        
        return cell0!
        case 1:
            let cell1 = Bundle.main.loadNibNamed("UserActivityTableViewCell", owner: self, options: nil)?.first as? UserActivityTableViewCell
            cell1?.separatorInset = UIEdgeInsets.zero
            cell1?.layoutMargins = UIEdgeInsets.zero
            return cell1!
            
        default :
            
            let cell2 = Bundle.main.loadNibNamed("SubcribeTableViewCell", owner: self, options: nil)?.first as? SubcribeTableViewCell
            cell2?.secondViewDeveloperLabel.text = Developer
            cell2?.separatorInset = UIEdgeInsets.zero
            cell2?.layoutMargins = UIEdgeInsets.zero
            return cell2!
        }
        
    }


}
