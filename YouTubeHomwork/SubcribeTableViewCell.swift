//
//  SubcribeTableViewCell.swift
//  YouTubeHomwork
//
//  Created by SreiNin Bo on 12/13/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class SubcribeTableViewCell: UITableViewCell {

    @IBOutlet weak var ImageLogo: UIImageView!
    @IBOutlet weak var secondViewDeveloperLabel: UILabel!
    
    @IBOutlet weak var subcribeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ImageLogo.layer.masksToBounds = true
        self.ImageLogo.layer.cornerRadius = (self.ImageLogo.frame.width)/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
